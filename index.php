<?php

// Load configuration
require 'config.php';

// Load libraries
require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use setasign\Fpdi\Tcpdf\Fpdi;

// Load templates
$header      = file_get_contents(TEMPLATES_DIR . 'header.html'      );
$footer      = file_get_contents(TEMPLATES_DIR . 'footer.html'      );
$confirm     =                   TEMPLATES_DIR . 'confirm.php'       ;
$form_header = file_get_contents(TEMPLATES_DIR . 'form_header.html' );
$form        =                   TEMPLATES_DIR . 'form.php'          ;

// Check if all templates got loaded succesfully.
// If not throw an error.
if ( $header && $footer && $form && $confirm &&$form_header ) {
	// Show form if there isn't any POST data, this
	// is the first time a user loads the web-page
	if ( empty($_POST) ) {
		echo $header;
		echo $form_header;
		require $form;
		echo $footer;
	}
	// There is POST data, a submission has been made.
	// The POST data and file need to be verified and
	// an email has to be sent to the treasurer.

	// If the data is incorect, a list of errors needs
	// to be shown.
	else {
		$post = $_POST;
		$post = cleanPostVariables($post);
		$validation_status = validateAll($post, $_FILES['ticket']);
		// Check if the amount of keys that hold the value
		// true is equal to the total amount of keys.
		// If yes; the submit is succesul.
		if ( count( array_keys($validation_status, true) )
					== count( $validation_status ) ) {
			handleSubmit($post, $_FILES['ticket']);
			echo $header;
			require $confirm;
		}
		else {
			// TODO: Show the relevant errors according to the
			// validation functions.

			$errors = array_keys($validation_status, false);

			// Build the page like normal, but insert the
			// relevant error messages
			echo $header;
			echo $form_header;
			foreach ($errors as $error) {
				switch($error) {
					case "name":
						echo alertMessage("Vul je naam in.");
						break;
					case "email":
						echo alertMessage("Emailadres niet geldig.");
						break;
					case "amount":
						echo alertMessage("Het bedrag moet een geldig cijfer zijn.");
						break;
					case "dateactivity":
						echo alertMessage("Vermeld de datum en activiteit.");
						break;
					case "description":
						echo alertMessage("Vermeld wat je gekocht hebt.");
						break;
					case "purpose":
						echo alertMessage("Vermeld waarvoor je het gekocht hebt.");
						break;
					case "bank-account":
						echo alertMessage("Ongeldig rekeningnummer.");
						break;
					case "ticket":
						$maxsize = (FILE_MAX_FILESIZE / 2**20) . "MB";
						echo alertMessage("Het bonnetje kan maximaal $maxsize groot zijn. Alleen .pdf, .png, en .jpg bestanden mogen worden geupload.");
						break;
					case "accept-tos":
						echo alertMessage("Je moet alles eerst checken voordat je een declaratie kan doen.");
						break;
					default:
						echo alertMessage("Je hebt een fout gemaakt, probeer het opnieuw.");
				}
			}
			require $form;
			echo $footer;
		}
	}
} else {
	echo "500 Internal Server Error";
	header("HTTP/1.1 500 Internal Server Error");
	exit;
}

function cleanPostVariables($post) {
	// Remove any empty amount/descriptions
	foreach ($post['description'] as $key => $description) {
		if($description === "") {
			unset($post['description'][$key]);
		}
	}
	foreach ($post['amount'] as $key => $description) {
		if($description === "") {
			unset($post['amount'][$key]);
		}
	}
	return $post;
}


// Validation functions. Used when a POST has
// been made to see if all the data is okay.

// This function returns a key-value array
// with bools that are used in creating the
// error messages.
function validateAll($post, $files) {
	// Array that holds the validation status
	// of each of the submitted values.
	$validation_status = array(
		"name"         => false,
		"email"        => false,
		"amount"       => false,
		"dateactivity" => false,
		"description"  => false,
		"purpose"      => false,
		"bank-account" => false,
		"remarks"      => true,
		"ticket"       => false,
		"accept-tos"   => false
	);

	// Assign correct truth value to all keys
	// in the validationstatus array. Remarks
	// are always in the right format.

	// TODO: find a metaprogramming way around
	// listing all this stuff manually. Preferably
	// involving standard function naming.
	$validation_status['email']        = validateEmail ($post['email']    );
	$validation_status['amount']       = validateAmount($post['amount']);
	$validation_status['dateactivity'] = !empty($post['dateactivity']);
	$validation_status['name']         = !empty($post['name']            );
	$validation_status['description']  = !empty($post['description']     ) && count($post['description']) === count($post['amount']) && count($post['description']) > 0;
	$validation_status['purpose']      = !empty($post['purpose']         );
	$validation_status['bank-account'] = validateIBAN($post['bank-account']);
	$validation_status['ticket']       = validateTickets($files);
	$validation_status['accept-tos']   = $post['accept-tos'];

	return $validation_status;
}

// Transnumerate A..Z in an IBAN to 10..35 (case insensitive)
function transnumerate($input) {
	$result = "";

	foreach ($input as $char) {
		$code = ord($char);

		if ($code >= 48 && $code <= 57) { // ord('0') and ord('9')
			$result .= $char;
		} else { // Character
			$code &= ~32; // Force uppercase
			$result .= (string)$code - 55; // -65, +10 means A (65) becomes 10
		}
	}

	return $result;
}

// Validate IBAN using regexp
function validateIBAN($IBAN) {
	// Validate layout
	if(!preg_match('/([a-zA-Z]{2}[0-9]{2})([a-zA-Z]{4}[0-9]{10})/', $IBAN, $matches))
		return false;

	// Move country code and checksum to end
	$transposed = $matches[2] . $matches[1];

	// Transliterate into numbers, where A = 10, B = 11 .. Z = 35
	$transliterated = transnumerate(str_split($transposed));

	return bcmod($transliterated, "97") == "1"; // Magic IBAN constants
}

// Provides escaping and wrapping in value='' for values that failed validation.
// value=''-wrapping is controlled by $attribute.
function refill($field, $attribute=true) {
	if (isset($_POST[$field])) {
		$fill = htmlspecialchars($_POST[$field], ENT_QUOTES | ENT_HTML5); // Replaces ', ", <, >, &
		if ($attribute)
			$fill = 'value="'. $fill .'"';
		return $fill;
	}
	return "";
}

// Validate email using the built in PHP
// filter. Does not support TLD addresses.
function validateEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL)
			&& preg_match('/@.+\./', $email);
}

function validateTickets($files) {
	$success = true;
	for($i = 0; $i < count($files['name']); $i++) {
		$success = $success && validateTicket($files, $i);
	}
	return $success;
}

// Validates wheter the uploaded file is of
// the correct type, size and has the correct
// extension.
function validateTicket($file, $index) {
	// The ticket failed validation if there are
	// any errors with it.
	if ($file['error'][$index] > 0) {
		return false;
	}

	// The original file extension is the last item
	// of the array of the original filename split
	// by periods.
	global $allowed_filetypes;

	// Check the mime type from the file itself
	$mimetype = new finfo(FILEINFO_MIME_TYPE);
	$extension = array_search(
        	$mimetype->file(
			$file['tmp_name'][$index]),
		$allowed_filetypes,
        	true
    	);

	if(!$extension){
		return false;
	}

	// Globalify the extension, this should be improved, cause it is ugly.
	global $extension;

	// A file is valid if it has an allowed extension,
	// an allowed filetype and is less than or equal
	// in size to the maximum filesize.
	if($file['size'][$index] <= FILE_MAX_FILESIZE ){
		return true;
	}
	return false;
}

// Validates whether the amount is numeric.
// Since the PHP is_numeric returns false on
// numbers with a comma instead of a dot to
// seperate decimals, we need a function instead
// of using the builtin.
function validateAmount($amounts) {
	$valid = true;
	foreach ($amounts as $amount) {
		$amount = str_replace(',', '.', $amount);
		$valid = $valid && is_numeric($amount);
	}
	return $valid;
}

function calculateTotalAmount($amounts) {
	$total = 0;
	foreach ($amounts as $amount) {
		$total += $amount;
	}
	return $total;
}

// Generates a DOM string conaining a warning
// message.
function alertMessage($string) {
	return '<div class="alert alert-warning">' . $string . '</div>';
}

function generatePdfDeclaration($post) {
	$totalAmount = calculateTotalAmount($post['amount']);
	$pdf = new Fpdi();
	$pdf->AddPage();
	$pdf->setSourceFile(TEMPLATES_DIR . "/pdf/declaratieformulier.pdf");
	$tplId = $pdf->importPage(1);
	$pdf->useTemplate($tplId, 0, 0, null, null, true);
	$pdf->SetTextColor(0, 0, 0);
	$pdf->setXY(73, 55);
	$pdf->Write(0, $post['name']);
	$pdf->setXY(73, 64);
	$pdf->Write(0, $post['purpose']);
	$pdf->setXY(73, 73);
	$pdf->Write(0, $post['dateactivity']);
	$currentY = 101;
	foreach ($post['description'] as $key => $description) {
		$amount = number_format(floatval($post['amount'][$key]), 2, '.', '');
		$pdf->setXY(20, $currentY);
		$pdf->Write(0, $description);
		$beforePoint = substr($amount, 0, -3);
		$afterPoint = substr($amount, -2);
		$pdf->setXY(170, $currentY);
		$pdf->Write(0, $beforePoint);
		$pdf->setXY(182, $currentY);
		$pdf->Write(0, $afterPoint);
		$currentY += 16;
	}
	$totalAmount = number_format(floatval($totalAmount), 2, '.', '');
	$beforePoint = substr($totalAmount, 0, -3);
	$afterPoint = substr($totalAmount, -2);
	$pdf->setXY(170, 181);
	$pdf->Write(0, $beforePoint);
	$pdf->setXY(182, 181);
	$pdf->Write(0, $afterPoint);
	$pdf->setXY(57, 200);
	$pdf->Write(0, $post['bank-account']);
	$pdf->setXY(57, 210);
	$pdf->Write(0, $post['name']);
	return $pdf->Output('test.pdf', 'S');
}

// Logic to handle a successfull submit.
// An email get's send to the treasurer
// with the ticket attached.
function handleSubmit($post, $files) {
	$totalamount = calculateTotalAmount($post['amount']);
	// Build the message body
	$treasurer = EMAIL_FIRST_NAME;
	$mail_body      =
		"Hoi $treasurer,\n\n" .

		"Ik heb zojuist het DigiDecs formulier ingevuld. " .
		"Dit zijn mijn gegevens.\n\n" .

		"Naam: {$post['name']}\n" .
		"Email: {$post['email']}\n" .
		"Totaalbedrag: {$totalamount}\n" .
		"Waarvoor: {$post['purpose']}\n" .
		"Datum + Activiteit: {$post['dateactivity']}\n" .
		"Rekeningnummer: {$post['bank-account']}\n" .
		"Opmerkingen: {$post['remarks']}\n\n" .

		"Groetjes,\n\n" .

		"{$post['name']}";

	// Rename the files
	global $allowed_filetypes;

	$file_names = [];
	for($i = 0; $i < count($files['name']); $i++) {
		// Check the mime type from the file itself
		$mimetype = new finfo(FILEINFO_MIME_TYPE);
		$extension = array_search(
			$mimetype->file(
				$files['tmp_name'][$i]),
			$allowed_filetypes,
			true
		);


		$file_name = $files['tmp_name'][$i] . '.' . $extension;
		rename( $files['tmp_name'][$i], $file_name );
		$file_names[] = $file_name;
	}

	$pdfFile = generatePdfDeclaration($post);

	$mailer = new PHPMailer();
	$mailer->isSendmail();
	$mailer->setFrom('declaraties@a-eskwadraat.nl');
	$mailer->addReplyTo($post['email']);
	$mailer->addAddress(EMAIL_TO_ADDRESS);
	$mailer->Subject = EMAIL_SUBJECT_BASE . $post['purpose'];
	$mailer->msgHTML(nl2br($mail_body));
	foreach ($file_names as $file_name) {
		$mailer->addAttachment($file_name);
	}
	$mailer->addStringAttachment($pdfFile, 'declaratie.pdf');
	$mailer->send();
}
