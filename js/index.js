let current = 0;

$('document').ready(function() {
    add_item_row(true);
    $('#add_product_btn').on('click', function() {
        add_item_row();
    });
});


function add_item_row(required = false) {
    if(current >= 5) {
        alert("Er kunnen max 5 producten worden toegevoegd!");
        return;
    }
    let new_item = $('#bought-row-to-copy').clone();
    new_item.prop('id', null);
    $('#div_item_container').append(new_item);
    new_item.find('input').prop('required', required);
    new_item.show();
    current++;
}