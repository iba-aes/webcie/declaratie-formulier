	<form role="form" action="index.php" method="post" enctype="multipart/form-data" id="digidecs-form">
		<div class="form-group">
			<label for="name">Naam<sup>*</sup></label>
			<input type="text" class="form-control" id="name" name="name" placeholder="Secretaris"
			<?php echo refill("name"); ?>
			required>
		</div>

                <div class="form-group">
                        <label for="bank-account">IBAN<sup>*</sup></label>
						<input id="bank-account" name="bank-account" type="text" class="form-control" placeholder="NL13TEST0123456789"
						<?php echo refill("bank-account"); ?>
						required>
                </div>

		<div class="form-group">
			<label for="email">E-mailadres<sup>*</sup></label>
			<input type="email" class="form-control" id="email" name="email" placeholder="test@example.com"
			<?php echo refill("email"); ?>
			required>
		</div>

		<div class="form-group">
			<label for="dateactivity">Datum + Activiteit<sup>*</sup></label>
			<input id="dateactivity" name="dateactivity" type="text" class="form-control" placeholder="23 maart - Bier drinken"
			<?php echo refill("dateactivity"); ?> required>
		</div>

        <div class="form-group">
            <div class="col-md-8 no-padding"><label>Wat heb je gekocht?<sup>*</sup></label></div>
            <div class="col-md-4 no-padding"><label>Kosten<sup>*</sup></label></div>
        </div>

        <div id="div_item_container">
            <div class="form-row no-gutters" id="bought-row-to-copy" style="display: none;">
                <div class="form-group col-md-8 no-padding">
                    <input id="description" name="description[]" type="text" class="form-control" placeholder="Google Glasses"
						<?php echo refill("description"); ?>>
                </div>
                <div class="form-group col-md-4 no-padding">
                    <input id="amount" name="amount[]" type="text" class="form-control" placeholder="9999.00" pattern="[\-+]?[0-9]*[.]?[0-9]+" oninvalid="setCustomValidity('Only use digits and separate them with a dot (.) if needed.')"
                           onchange="try{setCustomValidity('')}catch(e){}"
						<?php echo refill("totalamount"); ?>>
                </div>
            </div>

        </div>

        <div class="form-group">
            <button type="button" id="add_product_btn" class="btn btn-success">Voeg product toe</button>
        </div>

        <div class="form-group">
			<label for="purpose">Waarvoor/welke commissie?<sup>*</sup></label>
			<input id="purpose" name="purpose" type="text" class="form-control" placeholder="WebOps"
			<?php echo refill("purpose"); ?> required>
		</div>

		<div class="form-group">
			<label for="ticket">Bonnetje uploaden<sup>*</sup></label>
			<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
			<input name="ticket[]" type="file" id="ticket" required multiple>
			<p class="help-block">Maximale grootte is <?php echo FILE_MAX_FILESIZE / 2 ** 20; ?>MB. Alleen .pdf, .jpg, en .png bestanden. Je kan meerdere bestanden selecteren.</p>

                        <p class="help-block">Zorg dat de datum, het (btw) bedrag en de verschillende producten of diensten goed leesbaar zijn.</p>
		</div>

		<div class="form-group">
			<label for="remarks">Opmerkingen</label>
			<textarea name="remarks" class="form-control" rows="3" type="textarea" id="remarks"><?php echo refill('remarks', false); ?></textarea>
		</div>

		<div class="form-group">
			<label for="accept-tos">
				<input type="checkbox" name="accept-tos" id="accept-tos" required> Ik heb alles gecheckt en naar waarheid ingevuld<sup>*</sup>
			</label>
		</div>
		<p>Velden met een <sup>*</sup> zijn verplicht</p>
		<input type="submit" class="btn btn-success" value="Geef geld!">
	</form>
</div>
